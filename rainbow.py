import cv2
import sys
import json
from main import processImage
from storeImage import storeImage
from resizeImage import resizeImage

img = cv2.imread(sys.argv[1], 1)

img = resizeImage(img)
storeImage(img)
result_string = processImage(img, True, True)
#
# print(json.loads(result_string))

cv2.waitKey(0)
cv2.destroyAllWindows()