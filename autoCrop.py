import cv2
from drawResults import drawResults
from contourTree import ContourTree
from jsonGenerator import generateElementJson
import math

def sortByArea(contour):
    return cv2.contourArea(contour)

def autoCrop(input_image, contours, hierarchy, client_width=None, client_height=None):
    contours_to_draw = []
    hierarchy_to_draw = []

    rows = input_image.shape[0]
    cols = input_image.shape[1]

    client_width_ratio = 1
    client_height_ratio = 1

    if client_width is not None:
        client_width_ratio = client_width / cols

    if client_height is not None:
        client_height_ratio = client_height / rows

    for index, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if area < rows * cols / 50:
            continue
        contours_to_draw.append(contour)
        hierarchy_to_draw.append(hierarchy[0][index])

    contours_to_draw.sort(key = sortByArea, reverse=True)

    if len(contours_to_draw) == 0:
        return generateElementJson('0', 0, 0, cols, rows)

    appFrameContour = contours_to_draw[0]
    if cv2.contourArea(appFrameContour) >= rows * cols * .99:
        appFrameContour = contours_to_draw[1]

    x,y,w,h = cv2.boundingRect(appFrameContour)

    app_frame_x = math.floor(x * client_width_ratio)
    app_frame_y = math.floor(y * client_height_ratio)
    app_frame_w = math.floor(w * client_width_ratio)
    app_frame_h = math.floor(h * client_height_ratio)

    return generateElementJson('0', app_frame_x, app_frame_y, app_frame_w, app_frame_h)

    # drawResults(input_image, [appFrameContour])

