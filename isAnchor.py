from contourTreeNode import ContourTreeNode

def isAnchor(node):
    children = node.getChildren()

    if len(children) != 3:
        return False

    for child in children:
        if len(child.getContour()) != 3:
            return False

    return True