import cv2
import constants as const

def resizeImage(cv_image):
    img_width = cv_image.shape[1]
    resizeFactor = const.RESIZED_IMAGE_WIDTH / img_width

    return cv2.resize(cv_image, None, fx=resizeFactor, fy=resizeFactor)
