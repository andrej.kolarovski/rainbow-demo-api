import constants as const
import cv2
from nextAvailableFileId import getNextAvailableFileId

def storeImage(cv_image):
    fileId = getNextAvailableFileId()
    filename = const.UPLOAD_FOLDER + str(fileId) + '.jpg'

    cv2.imwrite(filename, cv_image)

