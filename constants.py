CONTOUR_HIERARCHY_PARENT_ID_INDEX = 3

EL_APP_FRAME = 0
EL_MENU_BUTTON = 1
EL_BUTTON = 2
EL_IMAGE = 3
EL_INPUT = 4
EL_ANCHOR = 5

APPROX_POLY_DP_EPSILON = 4

ALLOWED_FILE_TYPES = set(['image/png', 'image/jpeg'])
UPLOAD_FOLDER = './uploaded_images/'

# per cent of app frame width for y axis or height for x axis
MAGNET_AXIS_THRESH = .5

RESIZED_IMAGE_WIDTH = 500
