from contourTreeNode import ContourTreeNode
import constants as const
import common as cmn

class ContourTree:
    def __init__(self, contours, hierarchy):
        self.nodes = []
        self.setNodesFromContours(contours)
        self.hierarchy = hierarchy
        self.organizeNodes()

    def addNode(self, node):
        self.nodes.append(node);

    def setNodesFromContours(self, contours):
        for index, contour in enumerate(contours):
            self.addNode(ContourTreeNode(contour, index))

    def organizeNodes(self):
        for index, node in enumerate(self.nodes):
            if self.hierarchy[index][const.CONTOUR_HIERARCHY_PARENT_ID_INDEX] == -1:
                continue

            parent_index = self.hierarchy[index][const.CONTOUR_HIERARCHY_PARENT_ID_INDEX]

            parent = self.getNodeByIndex(parent_index)

            node.setParent(parent)
            parent.addChild(node)

    def getNodes(self):
        return self.nodes

    def getNodeByIndex(self, index):
        for node in self.nodes:
            if node.getIndex() == index:
                return node

    def getAppFrameNode(self):
        for node in self.nodes:
            if node.getLabel() == const.EL_APP_FRAME:
                return node
        return

    def getLabeledNodes(self):
        ret = []
        for node in self.nodes:
            if node.getLabel() > -1:
                ret.append(node)
        return ret


    def printTree(self):
        for node in self.nodes:
            print("\n\nindex: " + str(node.getIndex()) + "\n")
            if node.getParent():
                print("parent: " + str(node.getParent().getIndex()) + "\n")
            for child in node.getChildren():
                print("child: " + str(child.getIndex()) + "\n")

    def printDetected(self):
        print("Detected:\n")

        for node in self.nodes:
            if node.getLabel() == -1:
                continue

            print("Label: " + cmn.labelToString(node.getLabel()))
            print(node.getBoundingRect())
            print("\n\n")
