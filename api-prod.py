from flask import Flask, request
from flask_restful import Api
from flask_cors import CORS

from resourceProcessImage import ProcessImage
from resourceGenerateHtml import GenerateHtml
from resourceGetTemplates import GetTemplates

import constants as const
import json


app = Flask(__name__)
api = Api(app)
cors = CORS(app, resources={r"*": {"origins": "*"}})

api.add_resource(ProcessImage, '/process-image')

api.add_resource(GetTemplates, '/get-templates')

api.add_resource(GenerateHtml, '/generate-html')

if __name__ == '__main__':
    app.run(host='0.0.0.0')
