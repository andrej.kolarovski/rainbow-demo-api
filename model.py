import numpy as np
import tensorflow as tf
import cv2 as cv
import math
import tensorflow as tf

from jsonGenerator import generateElementJson, generateJson
from mapObjectsClassName import mapObjects

def readGraph(cv_image, client_width=None, client_height=None):
    # Read the graph.
    with tf.gfile.FastGFile('sisoje_osmi.pb', 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    with tf.Session() as sess:
        # Restore session
        sess.graph.as_default()
        tf.import_graph_def(graph_def, name='')

        # Read and preprocess an image.
        rows = cv_image.shape[0]
        cols = cv_image.shape[1]
        inp = cv.resize(cv_image, (300, 300))

        # Run the model
        out = sess.run([sess.graph.get_tensor_by_name('num_detections:0'),
                        sess.graph.get_tensor_by_name('detection_scores:0'),
                        sess.graph.get_tensor_by_name('detection_boxes:0'),
                        sess.graph.get_tensor_by_name('detection_classes:0')],
                        feed_dict={'image_tensor:0': cv_image.reshape(1, rows, cols, 3)})

        # Visualize detected bounding boxes.

        scores = out[1][0]
        bboxes = out[2][0]


        selected_indices = tf.image.non_max_suppression(
            bboxes,
            scores,
            # outputs all bboxes not grouped by overlapping so in our case we should make it huuuuuuuuge
            max_output_size=10000,
            iou_threshold=0.1,
            score_threshold=float('-inf'),
            name=None
        )

        ret_arr = []

        client_width_ratio = 1
        client_height_ratio = 1

        # temp, haven't yet trained network to recognize app frames
        # ret_arr.append(generateElementJson('1', 0, 0, cols * client_width_ratio, rows * client_height_ratio))

        if client_width is not None:
            client_width_ratio = client_width / cols

        if client_height is not None:
            client_height_ratio = client_height / rows

        for i in selected_indices.eval():
            classId = int(out[3][0][i])
            score = float(scores[i])
            bbox = [float(v) for v in bboxes[i]]
            if score > 0.8:
                x = math.floor(bbox[1] * cols * client_width_ratio)
                y = math.floor(bbox[0] * rows * client_height_ratio)
                right = math.floor(bbox[3] * cols * client_width_ratio)
                bottom = math.floor(bbox[2] * rows * client_height_ratio)
                json_str = generateElementJson(str(classId), x, y, right - x, bottom - y)
                ret_arr.append(json_str)

        return ret_arr