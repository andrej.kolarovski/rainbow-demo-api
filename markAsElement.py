from contourTreeNode import ContourTreeNode
import cv2

def markAsElement(node, label):
    node.setLabel(label)

    boundingRect = cv2.boundingRect(node.getContour())
    node.setBoundingRect(boundingRect)