import cv2
import constants as const

def convertToGrayAndBlurImage(src_img, drawGray):
    src_gray = cv2.cvtColor(src_img, cv2.COLOR_BGR2GRAY)
    # src_gray = cv2.blur(src_gray, (1, 1))
    # src_gray = cv2.GaussianBlur(src_gray, (1, 1), 0)
    src_gray = cv2.adaptiveThreshold(src_gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 75, 10)
    src_gray = cv2.bitwise_not(src_gray)

    if not drawGray:
        return src_gray

    cv2.namedWindow("grayed", cv2.WINDOW_NORMAL)
    cv2.imshow("grayed", src_gray)

    return src_gray

def detectEdgesAndContours(src_gray):
    thresh = 1

    output, threshold_output = cv2.threshold(src_gray, thresh, 255, cv2.THRESH_BINARY)
    im2, contours, hierarchy = cv2.findContours(threshold_output, cv2.RETR_TREE, cv2.CHAIN_APPROX_TC89_KCOS)

    approximations = []

    for contour in contours:
        approx = cv2.approxPolyDP(contour, const.APPROX_POLY_DP_EPSILON, True)
        approximations.append(approx)

    return approximations, hierarchy

def detect(src_img, drawGray):
    src_gray = convertToGrayAndBlurImage(src_img, drawGray)
    return detectEdgesAndContours(src_gray)