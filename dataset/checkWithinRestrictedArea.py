def checkWithinRestrictedArea(tl, br, restricted_tl, restricted_br):
    if tl[0] > restricted_br[0] or restricted_tl[0] > br[0]:
        return False

    if tl[1] > restricted_br[1] or restricted_tl[1] > br[1]:
        return False

    return True