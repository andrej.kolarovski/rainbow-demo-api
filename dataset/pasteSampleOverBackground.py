from PIL import Image
import random
import math
from pascal_voc_writer import Writer
from checkWithinRestrictedArea import checkWithinRestrictedArea

def pasteSample(class_name, sample, neg_sample, xml_writer, min_x, min_y, max_x, max_y, pasteInBottom=False, restricted_tl=(-1, -1), restricted_br=(-1, -1)):
    sample_w, sample_h = sample.size
    neg_w, neg_h = neg_sample.size

    max_x -= sample_w
    max_y -= sample_h

    tl = (-1, -1)
    br = (-1, -1)

    allowedInArea = False
    triesCount = 0

    while (allowedInArea == False and triesCount < 50):
        triesCount += 1

        paste_x = random.randint(min_x, max_x - 1)
        paste_y = random.randint(min_y, max_y - 1)

        tl = (paste_x, paste_y)
        br = (paste_x + sample_w, paste_y + sample_h)

        if restricted_tl[0] == -1:
            allowedInArea = True
        else:
            allowedInArea = not checkWithinRestrictedArea(tl, br, restricted_tl, restricted_br)

    if not allowedInArea:
        return neg_sample, tl, br

    # RGBA
    neg_sample.paste(sample, tl, sample)

    # RGB
    # neg_sample.paste(sample, tl)

    xml_writer.addObject(class_name, tl[0], tl[1], br[0], br[1])

    return neg_sample, tl, br