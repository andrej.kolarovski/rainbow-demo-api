from PIL import Image
import random
import math
import json

def pasteFrame(filename, frame, image):
    json_filename = filename.replace('.jpg', '.json').replace('.png', '.json')

    with open(json_filename) as json_file:
        data = json.load(json_file)

        pad_left = data['padded_left']
        pad_right = data['padded_right']

        pad_top = data['padded_top']
        pad_bottom = data['padded_bottom']

        frame_w, frame_h = frame.size
        image_w, image_h = image.size

        offset_x = math.floor((image_w - frame_w) / 2)
        offset_y = math.floor((image_h - frame_h) / 2)

        max_x = offset_x + pad_right
        max_y = offset_y + pad_bottom

        offset = (offset_x, offset_y)

        image.paste(frame, offset, frame)


        return image, offset_x + pad_left, offset_y + pad_top, max_x, max_y
