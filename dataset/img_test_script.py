import cv2 as cv
import csv

with open('train.csv') as csvfile:
    rows_to_test = []
    csv_data = csv.reader(csvfile, delimiter=',', quotechar='|')

    current_filepath = ''
    img = ''

    for index, row in enumerate(csv_data):
        if (index == 0):
            continue
        filepath = row[0]
        if (filepath != current_filepath):
            if (current_filepath != ''):
                cv.imwrite('./test_script_output/' + current_filepath.split('/')[8], img)
                print('parsed: ' + current_filepath)

            current_filepath = filepath
            img = cv.imread(current_filepath)

        min_x = row[4]
        min_y = row[5]
        max_x = row[6]
        max_y = row[7]
        classname = row[3]

        cv.rectangle(img, (int(min_x), int(min_y)), (int(max_x), int(max_y)), (125, 255, 51), thickness=2)

        cv.putText(img,classname,(int(min_x), int(min_y)), cv.FONT_HERSHEY_SIMPLEX, 0.3,(0,0,255),1,cv.LINE_AA)
