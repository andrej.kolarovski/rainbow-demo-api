from PIL import Image
from pasteSampleOverBackground import pasteSample
from pasteFrame import pasteFrame
from pascal_voc_writer import Writer
import os.path
import random

pos_samples_path = './imput/pozitivni/'
neg_samples_path = './imput/negativni/'
img_output_path = './imput/kombinovani/img/'
xml_output_path = './imput/kombinovani/xml/'
xml_img_relative_path = '../img/'
frames_path = './imput/helpers/'

samples_count = 404
i = 351

start_from_background = 36
backgrounds_count = 39

start_from_frame = 6
frames_count = 12

while i <= samples_count:
    sample_index = str(i)

    img_sample_path = pos_samples_path + 'image/img_' + sample_index + '.jpg'
    if not os.path.isfile(img_sample_path):
        img_sample_path = img_sample_path.replace('.jpg', '.png')

    btn_sample_path = pos_samples_path + 'button/btn_' + sample_index + '.jpg'
    if not os.path.isfile(btn_sample_path):
        btn_sample_path = btn_sample_path.replace('.jpg', '.png')

    bgnd_index = str(random.randint(start_from_background, backgrounds_count))
    bgnd_path = neg_samples_path + 'bg_' + bgnd_index + '.jpg'
    if not os.path.isfile(bgnd_path):
        bgnd_path = bgnd_path.replace('.jpg', '.png')

    frame_index = str(random.randint(start_from_frame, frames_count))
    frame_path = frames_path + 'frame_' + frame_index + '.jpg'
    if not os.path.isfile(frame_path):
        frame_path = frame_path.replace('.jpg', '.png')

    print(bgnd_path)

    pos_img_sample = Image.open(img_sample_path)
    pos_btn_sample = Image.open(btn_sample_path)
    neg_sample = Image.open(bgnd_path)
    frame_sample = Image.open(frame_path)

    writer = Writer(img_output_path + 'f_' + sample_index + '.jpg', neg_sample.size[0], neg_sample.size[1])

    neg_sample, tl_x, tl_y, br_x, br_y = pasteFrame(frame_path, frame_sample, neg_sample)
    neg_sample, restricted_tl, restricted_br = pasteSample('image', pos_img_sample, neg_sample, writer, tl_x, tl_y, br_x, br_y)
    neg_sample, restricted_tl, restricted_br = pasteSample('button', pos_btn_sample, neg_sample, writer, tl_x, tl_y, br_x, br_y, True, restricted_tl, restricted_br)

    neg_sample.convert('RGB')
    neg_sample.save(img_output_path + 'f_' + sample_index + '.jpg')
    writer.save(xml_output_path + 'f_' + sample_index + '.xml')

    print('Done sample ' + sample_index)

    i += 1