import constants as const
import os
import glob

def getFileId(filepath):
    fileId = int(filepath.replace(const.UPLOAD_FOLDER, '').split('.')[0])
    return fileId

def getNextAvailableFileId():
    uploadsDir = const.UPLOAD_FOLDER

    files = glob.glob(uploadsDir + "*")
    files.sort(key = getFileId)

    for index, filepath in enumerate(files):
        fileId = getFileId(filepath)

        if fileId != index:
            return index

    return len(files)