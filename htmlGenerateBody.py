def findAppFrame(elements):
    ret = ''
    for element in elements:
        if element['type'] == 'App Frame':
            ret = element
    return ret

def htmlGenerateBody(elements):
    appFrame = findAppFrame(elements)
    scr_width = appFrame['width']
    scr_height = appFrame['height']

    width_to_height_ratio = scr_width / scr_height

    ret = '<body style="position: relative; height: ' + str(width_to_height_ratio) + 'vw;">'
    ret +='</body>'

    return ret