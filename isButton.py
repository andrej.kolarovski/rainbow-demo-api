from contourTreeNode import ContourTreeNode

def isButton(node):
    children = node.getChildren()
    return len(children) == 0