from contourTreeNode import ContourTreeNode
import cv2
import common as cmn

# def generateElementJson(node):
#     br = node.getBoundingRect()
#
#     ret = "{\"type\": \""
#     ret += cmn.labelToString(node.getLabel()) + "\","
#     ret += "\"offsetX\": " + str(br[0]) + ", "
#     ret += "\"offsetY\": " + str(br[1]) + ", "
#     ret += "\"width\": " + str(br[2]) + ", "
#     ret += "\"height\": " + str(br[3])
#     ret += "},"
#
#     return ret

def generateElementJson(el_class, x, y, right, bottom):
    ret = "{\"type\": "
    ret += el_class + ","
    ret += "\"offsetX\": " + str(x) + ", "
    ret += "\"offsetY\": " + str(y) + ", "
    ret += "\"width\": " + str(right) + ", "
    ret += "\"height\": " + str(bottom)
    ret += "}"

    return ret

def generateJson(el_json_arr):
    ret_json = "{\"elements\": ["

    ret_json += ", ".join(el_json_arr)

    ret_json += "]}"

    return ret_json