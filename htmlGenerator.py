from dummyData import  dummyData
from htmlGenerateBody import htmlGenerateBody

def generateHtml(json):
    ret = '<html>'
    ret += htmlGenerateBody(dummyData)
    ret += '</html>'
    return ret