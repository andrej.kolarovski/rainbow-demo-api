from contourTreeNode import ContourTreeNode
from setAutoAlignAxes import setLeftYMagnetAxes, setRightYMagnetAxes, setTopXMagnetAxes, setBottomXMagnetAxes
from autoAlignBoundingRectangles import autoAlignBoundingRectangles
import constants as const
import cv2

def autoAlignRectangles(rectangles, app_window_width, app_window_height):
    x_thresh = (app_window_height / 100) * const.MAGNET_AXIS_THRESH
    y_thresh = (app_window_width / 100) * const.MAGNET_AXIS_THRESH

    x_magnet_axes = []
    y_magnet_axes = []

    for rectangle in rectangles:
        y_magnet_axes = setLeftYMagnetAxes(rectangle, y_magnet_axes, y_thresh)
        y_magnet_axes = setRightYMagnetAxes(rectangle, y_magnet_axes, y_thresh)

        x_magnet_axes = setTopXMagnetAxes(rectangle, x_magnet_axes, x_thresh)
        x_magnet_axes = setBottomXMagnetAxes(rectangle, x_magnet_axes, x_thresh)

    return autoAlignBoundingRectangles(rectangles, x_magnet_axes, x_thresh, y_magnet_axes, y_thresh)

def autoAlign(nodes, appFrame):
    rectangles = []
    for node in nodes:
        rectangles.append(node.getBoundingRect())

    appWindowWidth = appFrame.getBoundingRect()[2]
    appWindowHeight = appFrame.getBoundingRect()[3]

    alignedRectangles = autoAlignRectangles(rectangles, appWindowWidth, appWindowHeight)
    for i, node in enumerate(nodes):
        node.setBoundingRect(alignedRectangles[i])