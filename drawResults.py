import cv2

def drawResults(img, contours):
    drawing = img.copy()
    output = img.copy()

    cv2.drawContours(output, contours, -1, (0,255,0), 1)

    cv2.addWeighted(drawing, 0.1, output, 0.9, 0, output)

    cv2.namedWindow("detected", cv2.WINDOW_NORMAL)
    cv2.imshow("detected", output)

