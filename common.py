import constants as const

def el_menu_button():
    return "Menu Button"

def el_button():
    return "Button"

def el_image():
    return "Image"

def el_app_frame():
    return "App Frame"

def el_input():
    return "Input"

def el_anchor():
    return "Anchor"

switcher = {
    const.EL_APP_FRAME: el_app_frame,
    const.EL_BUTTON: el_button,
    const.EL_MENU_BUTTON: el_menu_button,
    const.EL_IMAGE: el_image,
    const.EL_INPUT: el_input,
    const.EL_ANCHOR: el_anchor,
}

def labelToString(label):
    func = switcher.get(label, "err")
    return func()

