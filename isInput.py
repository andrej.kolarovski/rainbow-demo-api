from contourTreeNode import ContourTreeNode

def isInput(node):
    children = node.getChildren()

    if len(children) != 1:
        return False

    for child in children:
        if len(child.getContour()) != 2:
            return False

    return True