import cv2

import common
from contourDetection import detect
from contourTree import ContourTree
from elementsDetection import labelNodes
from drawResults import drawResults
from jsonGenerator import generateJson
from autoAlign import autoAlign
from model import readGraph
from autoCrop import autoCrop


def processImage(img, client_width=None, client_height=None, shouldDrawResults=False, shouldDrawGray=False):
    contours, hierarchy = detect(img, shouldDrawGray)
    #
    # if shouldDrawResults:
    #     drawResults(img, contours)
    #
    # contourTree = ContourTree(contours, hierarchy[0])
    #
    # labelNodes(contourTree)
    #
    # labeledNodes = contourTree.getLabeledNodes()

    # autoAlign(labeledNodes, contourTree.getAppFrameNode())

    # return generateJson(labeledNodes)

    object_jsons = readGraph(img, client_width, client_height)
    frame_json = autoCrop(img, contours, hierarchy, client_width, client_height)

    object_jsons.append(frame_json)

    ret_json = generateJson(object_jsons)
    return ret_json
