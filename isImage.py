from contourTreeNode import ContourTreeNode

def isImage(node):
    children = node.getChildren()

    if len(children) != 4:
        return False

    for child in children:
        if len(child.getContour()) != 3:
            return False

    return True