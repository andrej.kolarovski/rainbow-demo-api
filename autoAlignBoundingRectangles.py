import cv2

def autoAlignBoundingRectangles(rectangles, x_magnet_axes, x_thresh, y_magnet_axes, y_thresh):
    for rectangle in rectangles:
        leftX = rectangle[0]
        topY = rectangle[1]
        width = rectangle[2]
        height = rectangle[3]

        for axis in x_magnet_axes:
            if abs(leftX - axis) <= x_thresh:
                leftX = axis

        for axis in x_magnet_axes:
            if abs((leftX + width) - axis) <= x_thresh:
                width = axis - leftX

        for axis in y_magnet_axes:
            if abs(topY - axis) <= y_thresh:
                topY = axis

        for axis in y_magnet_axes:
            if abs((topY + height) - axis) <= x_thresh:
                height = axis - topY

        rectangle = (leftX, topY, width, height)

    return rectangles