from contourTreeNode import ContourTreeNode

def isMenuButton(node):
    children = node.getChildren()

    if len(children) != 3:
        return False

    for child in children:
        if len(child.getContour()) != 2:
            return False

    return True