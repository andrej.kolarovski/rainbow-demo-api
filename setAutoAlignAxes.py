import cv2

def setLeftYMagnetAxes(rectangle, y_magnet_axes, y_thresh):
    leftX = rectangle[0]

    if len(y_magnet_axes) == 0:
        y_magnet_axes.append(leftX)
        return y_magnet_axes

    for axis in y_magnet_axes:
        if abs(leftX - axis) <= y_thresh:
            axis = abs((leftX - axis) / 2)
            return y_magnet_axes

    y_magnet_axes.append(leftX)
    return y_magnet_axes

def setRightYMagnetAxes(rectangle, y_magnet_axes, y_thresh):
    rightX = rectangle[0] + rectangle[2]

    if len(y_magnet_axes) == 0:
        y_magnet_axes.append(rightX)
        return y_magnet_axes

    for axis in y_magnet_axes:
        if abs(rightX - axis) <= y_thresh:
            axis = abs((rightX - axis) / 2)
            return y_magnet_axes

    y_magnet_axes.append(rightX)
    return y_magnet_axes

def setTopXMagnetAxes(rectangle, x_magnet_axes, x_thresh):
    topY = rectangle[1]

    if len(x_magnet_axes) == 0:
        x_magnet_axes.append(topY)
        return x_magnet_axes

    for axis in x_magnet_axes:
        if abs(topY - axis) <= x_thresh:
            axis = abs((topY - axis) / 2)
            return x_magnet_axes

    x_magnet_axes.append(topY)
    return x_magnet_axes

def setBottomXMagnetAxes(rectangle, x_magnet_axes, x_thresh):
    bottomY = rectangle[1] + rectangle[3]

    if len(x_magnet_axes) == 0:
        x_magnet_axes.append(bottomY)
        return x_magnet_axes

    for axis in x_magnet_axes:
        if abs(bottomY - axis) <= x_thresh:
            axis = abs((bottomY - axis) / 2)
            return x_magnet_axes

    x_magnet_axes.append(bottomY)
    return x_magnet_axes