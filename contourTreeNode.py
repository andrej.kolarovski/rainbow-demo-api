class ContourTreeNode:
    def __init__(self, contour, index):
        self.contour = contour
        self.index = index
        self.label = -1
        self.children = []
        self.parent = None

    def addChild(self, child):
        self.children.append(child)

    def getChildren(self):
        return self.children

    def setParent(self, parent):
        self.parent = parent

    def getParent(self):
        return self.parent

    def getIndex(self):
        return self.index

    def getContour(self):
        return self.contour

    def getLabel(self):
        return self.label

    def setLabel(self, label):
        self.label = label

    def setBoundingRect(self, rect):
        self.boundingRect = rect

    def getBoundingRect(self):
        return self.boundingRect

