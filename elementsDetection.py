from contourTree import ContourTree
from isAppFrame import isAppFrame
from isButton import isButton
from isImage import isImage
from isMenuButton import isMenuButton
from isInput import isInput
from isAnchor import isAnchor
from markAsElement import markAsElement
import constants as const

def labelNodes(tree):
    for node in tree.getNodes():

        if len(node.getContour()) != 4:
            node.setLabel(-1)
            continue

        if isButton(node):
            markAsElement(node, const.EL_BUTTON)
            continue

        if isImage(node):
            markAsElement(node, const.EL_IMAGE)
            continue

        if isMenuButton(node):
            markAsElement(node, const.EL_MENU_BUTTON)
            continue

        if isInput(node):
            markAsElement(node, const.EL_INPUT)
            continue

        if isAnchor(node):
            markAsElement(node, const.EL_ANCHOR)
            continue

        if isAppFrame(node):
            markAsElement(node, const.EL_APP_FRAME)
            continue

        node.setLabel(-1)
