# Rainbow demo API

Demo version of how Rainbow's API is going to work. Basic shape recognition by counting angles, works only with precisely drawn shapes using straight lines.

## Getting Started

Clone the repository on your machine.

git clone https://gitlab.com/andrej.kolarovski/rainbow-demo-api.git

### Prerequisites

Python 3, OpenCV 3 for python (module's still called cv2, go figure), Flask (http://flask.pocoo.org/docs/1.0/installation/)

### Running

To start up a local server, run python api.py.

Send a POST request to http://127.0.0.1:5000/process-image.
Sample request payload JSON: {"file": <Base64_encoded_image>}

To just run the recognition process, run python rainbow.py <path_to_image>

