from templates import getTemplatesJson
from flask_restful import Resource

class GetTemplates(Resource):
    def get(self):
        return getTemplatesJson()