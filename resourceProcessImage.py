import json
from main import processImage
from base64convertor import stringToImage
from makeError import makeError
from fileCheck import allowedFile
from flask import request
from main import processImage
from flask_restful import Resource
from storeImage import storeImage
from resizeImage import resizeImage

class ProcessImage(Resource):
    def post(self):
        request_json = request.get_json()

        if not 'file' in request_json:
            return makeError(400, "Invalid payload: 'file' field missing.")

        input_file = request_json['file']

        client_width = None
        if 'client_width' in request_json:
            client_width = request_json['client_width']

        client_height = None
        if 'client_height' in request_json:
            client_height = request_json['client_height']

        if input_file.find(',') > -1:
            image_meta = input_file.split(',')[0]

            if not allowedFile(image_meta):
                return makeError(400, "Invalid file type: file must be must be png or jpeg")

            image_data = stringToImage(input_file.split(',')[1])
        else:
            image_data = stringToImage(input_file)

        image_data = resizeImage(image_data)

        # We store your sketches to create a dataset that Sisoje will learn from. Also to steal your souls using wodoo magic.
        storeImage(image_data)
        json_string = processImage(image_data, client_width, client_height)
        return json.loads(json_string)