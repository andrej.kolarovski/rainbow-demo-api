dummyData = [
    {
        "type": "App Frame",
        "offsetX": 0,
        "offsetY": 0,
        "width": 638,
        "height": 640
    },
    {
        "type": "Button",
        "offsetX": 376,
        "offsetY": 287,
        "width": 193,
        "height": 34,
        "buttonType": "Button",
        "placeholder":"My First Button",
        "clickHandler":"",
        "id":"btn_1"
    },
    {
        "type":"Button",
        "offsetX":73,
        "offsetY":284,
        "width":193,
        "height":35,
        "buttonType":"Submit",
        "placeholder":"Other Button",
        "clickHandler":"",
        "id":"btn_2"
    },
    {
        "type":"Image",
        "offsetX":372,
        "offsetY":62,
        "width":200,
        "height":190,
        "source":"https://www.overdriveonline.com/wp-content/uploads/sites/8/2017/07/Mollers-2017-07-25-08-33-e1500989599756.jpg",
        "id":"img_3"
    },
    {
        "type":"Image",
        "offsetX":70,
        "offsetY":62,
        "width":200,
        "height":189,
        "source":"https://www.bigmacktrucks.com/uploads/monthly_2018_08/132195242_Photo2.jpg.510b884c1a229c6ec3576222d3a92258.jpg",
        "id":"img_4"
    }
]