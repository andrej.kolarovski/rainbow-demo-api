import constants as const

def allowedFile(file_meta):
    metaData = file_meta.split(';')[0]
    fileType = metaData.split(':')[1]
    return fileType in const.ALLOWED_FILE_TYPES