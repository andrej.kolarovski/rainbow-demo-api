import base64
import io
import cv2
from imageio import imread
import matplotlib.pyplot as plt

def stringToImage(b64_string):
    img = imread(io.BytesIO(base64.b64decode(b64_string)))
    cv2_img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    return cv2_img
